##Comunicazione seriale tra stm32f4 e PC##
Per la comunicazione tra stm32f4 e PC possiamo usare la porta seriale, ma come descritto nel paragrafo 6.1.3 del datasheet della board:
Two solutions are possible to connect an STM32F407 USART to the VCP on the PC:
1. Using an USART to USB dongle from the market connected for instance to STM32F407 USART2 available on connector P1 pin 14 (PA2: USART2_TX) and P1 pin 13 (PA3: USART2_RX).
2. Using flying wires to connect ST-LINK/V2-A Virtual com port (ST-LINK VCP on U2 pin 12 and 13) to STM32F407 USART2 (PA2 and PA3: P1 pin 14 and 13)