using Microsoft.SPOT;
using System.Threading;
using STM32F407G_Discovery.Netmf.Models;
using STM32F407G_Discovery.Netmf.Utils;
using STM32Commons.Models;
using STM32Commons.Mapper;
using System;
using STM32Commons.Utils;

namespace STM32F407G_Discovery
{
    public class Program
    {
        public static void Main()
        {
            Board board = new Board();
            MessageHandler handler = new MessageHandler(board);
            Led green = new Led(Led.GREEN);
            Led red = new Led(Led.RED);
            while (true) {
                Debug.GC(true);
                string received = board.uart.ReadLine();
                Debug.Print("Received: " + received);
                green.off();
                red.off();

                Message request = null;
                Message response = null;

                try
                {
                    green.on();
                    red.off();
                    request = MessageMapper.deserialize(received);
                    response = handler.handleMessage(request);
                }
                catch (Exception ex)
                {
                    red.on();
                    green.off();
                    response = new Message();
                    response.Opcode = Commands.ERROR;
                    response.Payload = ex.Message;
                    response.Lenght = 1;
                }
                
                string tosend = MessageMapper.serialize(response);
                Debug.Print("Sending: " + tosend);
                board.uart.WriteLine(tosend);
                Thread.Sleep(100);
            }            
        }       

    }
}
