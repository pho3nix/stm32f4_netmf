/**
  ******************************************************************************
  * @file    STM32F407G_Discovery.Netmf.Models\Pin.cs
  * @author  Salvatore Alonge
  * @version V1.0.0
  * @date    31-Mar-2017
  ******************************************************************************
  */

using System;
using Microsoft.SPOT.Hardware;
using STM32F407G_Discovery.Netmf.Hardware;
using System.Collections;
using STM32Commons.Models;
using STM32Commons.Utils;

namespace STM32F407G_Discovery.Netmf.Models
{
    public class BoardPin : Pin
    {
        private Object port;

        public BoardPin(string pinName) : base(pinName)
        {
            ArrayList supportedModes = new ArrayList();
            if (GPIO.contains(pinName)) {
                supportedModes.Add(Modes.GPIO_INPUT);
                supportedModes.Add(Modes.GPIO_OUTPUT);
            }
            if (LED.contains(pinName))
            {
                supportedModes.Add(Modes.LED);
            }
            if (PWMS.contains(pinName))
            {
                supportedModes.Add(Modes.PWM);
            }
            if (ADC.contains(pinName))
            {
                supportedModes.Add(Modes.ANALOG_INPUT);
            }
            if (BUTTON.contains(pinName))
            {
                supportedModes.Add(Modes.BUTTON);
            }
            this.SupportedModes = supportedModes.ToArray(typeof(int)) as int[];
        }

        protected override void disable()
        {
            if (port != null)
            {
                if (Mode == Modes.GPIO_INPUT)
                {
                    InputPort ip = (InputPort)port;
                    ip.Dispose();
                }
                else if (Mode == Modes.GPIO_OUTPUT)
                {
                    OutputPort op = (OutputPort)port;
                    op.Dispose();
                }
                else if (Mode == Modes.ANALOG_INPUT)
                {
                    AnalogInput ai = (AnalogInput)port;
                    ai.Dispose();
                }
                else if (Mode == Modes.PWM)
                {
                    PWM pwm = (PWM)port;
                    pwm.Dispose();
                }
                port = null;
            }
            Mode = -1;
            Enabled = false;
        }

        protected override void enable()
        {
            if (port == null)
            {
                if (Mode == Modes.GPIO_INPUT)
                {
                    port = new InputPort(GPIO.get(Name), false, Port.ResistorMode.PullUp);
                }
                else if (Mode == Modes.GPIO_OUTPUT)
                {
                    port = new OutputPort(GPIO.get(Name), false);
                }
                else if (Mode == Modes.ANALOG_INPUT)
                {
                    port = new AnalogInput(ADC.get(Name));
                }
                else if (Mode == Modes.PWM)
                {
                    port = new PWM(PWMS.get(Name), 0.1, 0.1, false);
                }
                else
                {
                    throw new Exception("Unknown pin mode");
                }
                Enabled = true;
            }
        }
        
        private void setPWM(double freq, double dutycycle)
        {
            if (Mode != Modes.PWM) throw new Exception("Unsupported method for current pin mode");
            ((PWM)port).Frequency = freq;
            ((PWM)port).DutyCycle = dutycycle;
            ((PWM)port).Start();
        }

        private void setDigital(bool state)
        {
            if (Mode != Modes.GPIO_OUTPUT) throw new Exception("Unsupported method for current pin mode");
            ((OutputPort)port).Write(state);
        }

        public PinValue Read()
        {
            PinValue value = new PinValue();
            value.Name = this.Name;
            if (Mode == Modes.GPIO_INPUT)
            {
                value.Value = getDigital() ? 1 : 0;
            }
            else if (Mode == Modes.ANALOG_INPUT)
            {
                value.Value = getAnalog();
            }
            else
            {
                throw new Exception("Unsupported method for current pin mode");
            }
            return value;
        }

        private bool getDigital()
        {
            if (Mode != Modes.GPIO_INPUT) throw new Exception("Unsupported method for current pin mode");
            return ((InputPort)port).Read();
        }

        private double getAnalog()
        {
            if (Mode != Modes.ANALOG_INPUT) throw new Exception("Unsupported method for current pin mode");
            return ((AnalogInput)port).Read();
        }

        public void Write(double value)
        {
            if (Mode == Modes.GPIO_OUTPUT)
            {
                setDigital(value>0);
            }
            if (Mode == Modes.PWM)
            {
                //TODO
                //setPWM
            }
            throw new Exception("Unsupported method for current pin mode");
        }
        
    }
}
