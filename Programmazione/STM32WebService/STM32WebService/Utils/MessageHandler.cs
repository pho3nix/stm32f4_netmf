using System;
using STM32Commons.Models;
using STM32Commons.Mapper;
using static STM32WebService.Utils.SerialUtils;
using STM32Commons.Utils;

namespace STM32WebService.Utils
{
    class MessageHandler
    {
        SerialUtils serial;
        public MessageHandler(SerialUtils serial)
        {
            this.serial = serial;
        }

        public Message sendSync(Message request)
        {
            Message response = new Message();
            if (request == null || request.Opcode == null)
            {
                throw new Exception("Empty or invalid Request");
            }
            else
            {
                string requestString = MessageMapper.serialize(request);
                Console.WriteLine("Request: " + requestString);
                string responseString = serial.syncWrite(requestString);
                Console.WriteLine("Response: " + responseString);
                response = MessageMapper.deserialize(responseString);
            }
            if (response.Opcode == Commands.ERROR)
            {
                throw new Exception((string)response.Payload);
            }
            return response;
        }

        public void beginAsync(Message request, OnDataReceived listener)
        {
            if (request == null || request.Opcode == null)
            {
                throw new Exception("Empty or invalid Request");
            }
            else
            {
                string requestString = MessageMapper.serialize(request);
                Console.WriteLine("Request: " + requestString);
                serial.addDataReceivedListener(listener);
                serial.asyncWrite(requestString);
            }
        }

        public void endAsync(Message request)
        {
            if (request == null || request.Opcode == null)
            {
                throw new Exception("Empty or invalid Request");
            }
            else
            {
                string requestString = MessageMapper.serialize(request);
                Console.WriteLine("Request: " + requestString);
                serial.removeDataReceivedListener();
                serial.asyncWrite(requestString);
            }
        }
    }
}