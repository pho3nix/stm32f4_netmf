using System;
using System.Text;
using STM32Commons.Models;
using System.Collections;
using STM32Commons.Utils;

namespace STM32Commons.Mapper
{
    static class MessageMapper
    {
        private static char START_DELIMITER = '#';
        private static char END_DELIMITER = '#';

        private static char ARRAY_START_DELIMITER = '[';
        private static char ARRAY_END_DELIMITER = ']';

        private static char TABLE_START_DELIMITER = '{';
        private static char TABLE_END_DELIMITER = '}';

        private static char TYPE_SEPARATOR = '$';
        private static char SEPARATOR = '|';
        private static char SUB_SEPARATOR = '!';
        private static char COLON = ':';


        public static Message deserialize(string str)
        {
            Message message = new Message();
            if (str == null || str.Length == 0) throw new Exception("Void message received");
            string[] sb;
            try {
                int start = str.IndexOf(START_DELIMITER);
                int end = str.LastIndexOf(END_DELIMITER);
                if(start < 0 || end <0 || start >= end) new Exception("Wrong message sintax.");
                str = str.Substring(start+1, end-start-1);
                sb = str.Split(SEPARATOR);
                if (sb.Length!=3) throw new Exception("Wrong delimiter.");
                message.Opcode = sb[0];
                if(message.Opcode==null || message.Opcode.Length==0) throw new Exception("Void opcode.");
                message.Lenght = int.Parse(sb[1]);
                if (message.Lenght == 1)
                {
                    message.Payload = deserializePayload(sb[2]);
                }
                else if (message.Lenght > 1)
                {
                    if (sb[2].IndexOf(ARRAY_START_DELIMITER) != -1)
                    {
                        sb = sb[2].Split(ARRAY_START_DELIMITER, ARRAY_END_DELIMITER);
                        if (sb.Length != 3) throw new Exception("Wrong array delimiter.");
                        sb = sb[1].Split(SUB_SEPARATOR);
                        ArrayList array = new ArrayList();
                        foreach(string s in sb)
                        {
                            array.Add(deserializePayload(s));
                        }
                        message.Payload = array;
                    }
                }
            }
            catch (Exception e) {
                throw new Exception("Unknown message sintax. "+e.Message);
            }
            return message;
        }
        public static string serialize(Message message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(START_DELIMITER);
            sb.Append(message.Opcode);
            sb.Append(SEPARATOR);
            sb.Append(message.Lenght);
            sb.Append(SEPARATOR);
            if (message.Payload is ArrayList)
            {
                sb.Append(ARRAY_START_DELIMITER);
                ArrayList list = (ArrayList)message.Payload;
                for (int i=0; i<list.Count; i++)
                {
                    sb.Append(serializePayload(list[i]));
                    if (i < list.Count-1) sb.Append(SUB_SEPARATOR);
                }
                sb.Append(ARRAY_END_DELIMITER);
            }
            else
            {
                sb.Append(serializePayload(message.Payload));
            }
            sb.Append(END_DELIMITER);
            return sb.ToString();
        }

        private static Object deserializePayload(string payload)
        {
            if (payload == null || payload.Length == 0) return null;

            string[] sb = payload.Split(TYPE_SEPARATOR);
            if (sb.Length!=2) throw new Exception("Unknown payload sintax. Wrong type delimiter.");
            string type = sb[0];
            payload = sb[1];
            if (type.Equals(typeof(Pin).Name))
            {
                return PinMapper.deserialize(payload);
            }
            else if (type.Equals(typeof(PinValue).Name))
            {
                return PinMapper.deserializeValue(payload);
            }
            else if (type.Equals(typeof(string).Name))
            {
                return (string)payload;
            }
            return null;
        }

        private static string serializePayload(Object payload)
        {
            if (payload == null) return "";

            StringBuilder sb = new StringBuilder();
            if (payload is Pin)
            {
                sb.Append(typeof(Pin).Name);
                sb.Append(TYPE_SEPARATOR);
                sb.Append(PinMapper.serialize((Pin)payload));
            }
            else if (payload is PinValue)
            {
                sb.Append(typeof(PinValue).Name);
                sb.Append(TYPE_SEPARATOR);
                sb.Append(PinMapper.serializeValue((PinValue)payload));
            }
            else if (payload is string)
            {
                sb.Append(typeof(string).Name);
                sb.Append(TYPE_SEPARATOR);
                sb.Append((string)payload);
            }
            else
            {
                throw new Exception("Unknown payload type. Cannot serialize object.");
            }
            return sb.ToString();
        }
    }
}
