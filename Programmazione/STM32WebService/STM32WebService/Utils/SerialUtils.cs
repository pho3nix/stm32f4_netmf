﻿using System.IO.Ports;

namespace STM32WebService.Utils
{
    public class SerialUtils
    {
        private SerialPort com;
        private bool running = false;
        private readonly object syncLock = new object();

        public delegate void OnDataReceived(string data);
        private OnDataReceived onDataReceived;
        
        public SerialUtils(string port, int speed)
        {
            com = new SerialPort(port, speed, Parity.None, 8, StopBits.One);
            start();
        }

        private void start()
        {
            // Begin communications
            com.Open();
            running = true;
        }

        public string syncWrite(string str)
        {
            //lock (syncLock)
            {
                com.Write(str + "\r\n");
                return com.ReadTo("\r\n");
            }
        }

        public void asyncWrite(string str)
        {
            lock (syncLock)
            {
                com.Write(str + "\r\n");
            }
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Show all the incoming data in the port's buffer
            string readed = com.ReadExisting();
            onDataReceived?.Invoke(readed);
            //Console.WriteLine(readed);
            //Console.WriteLine("There is something to read!");
        }

        public void addDataReceivedListener(OnDataReceived listener)
        {
            // Attach a method to be called when there
            // is data waiting in the port's buffer
            onDataReceived = listener;
            com.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
        }

        public void removeDataReceivedListener()
        {
            // Attach a method to be called when there
            // is data waiting in the port's buffer
            com.DataReceived -= new SerialDataReceivedEventHandler(port_DataReceived);
            onDataReceived = null;
        }

        ~SerialUtils()  // destructor
        {
            stop();
        }

        private void stop()
        {
            if (com != null) {
                com.Close();
                running = false;
            }
        }
    }
}