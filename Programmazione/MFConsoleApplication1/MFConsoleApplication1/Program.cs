using System.Text;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Hardware.UsbClient;
using System.IO.Ports;
using System.Collections;
using System.Threading; // Needed for Sleep() function
using STM32F407G_Discovery.Netmf.Hardware;
using STM32F407G_Discovery.Netmf;
using System;

namespace MFConsoleApplication1
{
    public class Program
    {

        public static void Main()
        {
            Board board = new Board();
            
            board.getPin("PA4").setMode(Pin.MODES.GPIO_INPUT);
            board.getPin("PA3").setMode(Pin.MODES.GPIO_INPUT);

            board.getPin("PA9").setMode(Pin.MODES.LED).setState(true);
            board.getPin("PD5").setMode(Pin.MODES.LED).setState(false);
            board.getPin("PD12").setMode(Pin.MODES.LED).setState(true);
            board.getPin("PD13").setMode(Pin.MODES.LED).setState(true);
           
            //AnalogInput ia = new AnalogInput(Cpu.AnalogChannel.ANALOG_0);
            //double res = ia.Read();

            board.getPin("PA0").setMode(Pin.MODES.ANALOG_INPUT);
            board.getPin("PA1").setMode(Pin.MODES.ANALOG_INPUT);
            board.getPin("PA2").setMode(Pin.MODES.ANALOG_INPUT);

            board.getPin("PD14").setMode(Pin.MODES.PWM).setPWM(5000000, 0.2);
            board.getPin("PD15").setMode(Pin.MODES.PWM).setPWM(5000000, 0.2);
            
            while (true) {
                string a = board.uart.ReadLine();
                Debug.Print("----------------\n");
                //Debug.Print("readed: "+a);
                board.uart.Write(board.readAllPins());
                Thread.Sleep(2000);
            }            

            //LED blue = new LED(LED.BLUE);
            //LED red = new LED(LED.RED);
            //LED yellow = new LED(LED.YELLOW);
            //LED green = new LED(LED.GREEN);

            //UART serial = new UART(UART.COM1, 9600);
            //serial.Write("Test UART 1");
            //double dc = 0.001;
            //PWM pwm = new PWM(Cpu.PWMChannel.PWM_0, 100, dc, false);
            //PWM pwm1 = new PWM(Cpu.PWMChannel.PWM_1, 100, dc, false);
            
            //bool z = false;
            /*while (true)
            {
                if (dc >= 1)
                    z = true;
                if (dc <= 0)
                    z = false;

                if (z)
                    dc = dc - 0.001;
                else
                    dc = dc + 0.001;
                //blue.toggle();
                pwm.DutyCycle = dc;
                pwm1.DutyCycle = dc;
                pwm.Start();
                pwm1.Start();
                Thread.Sleep(10);
                blue.toggle();
            }*/
        }       

    }
}
