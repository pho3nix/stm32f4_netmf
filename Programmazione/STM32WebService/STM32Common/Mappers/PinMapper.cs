using System;
using System.Text;
using STM32Commons.Models;
using System.Collections;

namespace STM32Commons.Mapper
{
    static class PinMapper
    {

        private static char SEPARATOR = ';';
        private static char ARRAY_SEPARATOR = '_';
        
        public static Pin deserialize(string str)
        {
            Pin pin = null;
            if (str == null || str.Length == 0) throw new Exception("No pin received");
            string[] sb;
            try {
                string name = null;
                int[] supportedModes = null;
                sb = str.Split(SEPARATOR);
                if (sb.Length < 1) throw new Exception("Unknown pin sintax. Wrong delimiter.");
                name = sb[0];
                if (sb.Length > 1)
                {
                    pin.Mode = int.Parse(sb[1]);
                    pin.Frequency = int.Parse(sb[2]);
                    if (sb.Length > 3)
                    {
                        sb = sb[4].Split(ARRAY_SEPARATOR);
                        ArrayList listModes = new ArrayList();
                        foreach (string mode in sb)
                        {
                            if (mode.Length > 0)
                            {
                                listModes.Add(int.Parse(mode));
                            }
                        }
                        supportedModes = listModes.ToArray(typeof(int)) as int[];
                    }
                }
                pin = new Pin(name, supportedModes);
            }
            catch (Exception e) {
                throw new Exception("Unknown pin sintax. " + e.Message);
            }
            return pin;
        }

        public static PinValue deserializeValue(string str)
        {
            PinValue value = new PinValue();
            if (str == null || str.Length == 0) throw new Exception("No value received");
            string[] sb;
            try
            {
                sb = str.Split(SEPARATOR);
                if (sb.Length < 2) throw new Exception("Unknown sintax. Wrong delimiter.");
                value.Name = sb[0];
                value.Value = double.Parse(sb[1]);
            }
            catch (Exception e)
            {
                throw new Exception("Unknown sintax. " + e.Message);
            }
            return value;
        }

        public static string serialize(Pin pin)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(pin.Name);
            if (pin.Mode != 0)
            {
                sb.Append(SEPARATOR);
                sb.Append(pin.Mode);

                sb.Append(SEPARATOR);
                sb.Append(pin.Frequency);

                if (pin.SupportedModes != null)
                {
                    sb.Append(SEPARATOR);
                    int i = 1;
                    foreach (int mode in pin.SupportedModes)
                    {
                        sb.Append(mode);
                        if (i < pin.SupportedModes.Length) sb.Append(ARRAY_SEPARATOR);
                        i++;
                    }
                }
            }
            return sb.ToString();
        }

        public static string serializeValue(PinValue value)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(value.Name);
            sb.Append(SEPARATOR);
            sb.Append(value.Value);
            return sb.ToString();
        }

    }
}
