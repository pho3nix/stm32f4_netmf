using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace STM32F407G_Discovery.Netmf.Models
{
    class Led
    {
        public const Cpu.Pin GREEN_USB = (Cpu.Pin)9;
        public const Cpu.Pin RED_USB = (Cpu.Pin)53;

        public const Cpu.Pin GREEN = (Cpu.Pin)60;
        public const Cpu.Pin YELLOW = (Cpu.Pin)61;
        public const Cpu.Pin RED = (Cpu.Pin)62;
        public const Cpu.Pin BLUE = (Cpu.Pin)63;

        private OutputPort led;
        private bool status;

        public Led(Cpu.Pin pinLed)
        {
            led = new OutputPort(pinLed, false);
            status = false;
        }

        public bool getStatus()
        {
            return status;
        }

        public void toggle()
        {
            status = !status;
            update();
        }

        public void on()
        {
            status = true;
            update();
        }

        public void off()
        {
            status = false;
            update();
        }

        private void update()
        {
            led.Write(status);
        }
    }
}
