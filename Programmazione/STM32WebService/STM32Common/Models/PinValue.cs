﻿namespace STM32Commons.Models
{
    public class PinValue
    {
        public string Name { get; set; }
        public double Value { get; set; }
    }
}
