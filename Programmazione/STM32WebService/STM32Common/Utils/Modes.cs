namespace STM32Commons.Utils
{
    public static class Modes
    {
        public const int LED = 1;
        public const int GPIO_OUTPUT = 1;

        public const int BUTTON = 2;
        public const int GPIO_INPUT = 2;
        
        public const int ANALOG_INPUT = 3;
        public const int PWM = 4;
        public const int UART = 5;
    }
}
