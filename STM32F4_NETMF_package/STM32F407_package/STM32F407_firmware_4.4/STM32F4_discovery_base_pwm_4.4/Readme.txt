DeviceInfo:
  HAL build info: 4.4.0.0, STM32F4DISCOVERY by CW2
  OEM Product codes (vendor, model, SKU): 255, 0, 65535
  Serial Numbers (module, system):
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
  Solution Build Info: 4.4.0.0, STM32F4DISCOVERY by CW2
  AppDomains:
    default, id=1
  Assemblies:
    mscorlib,4.4.0.0
    Microsoft.SPOT.Native,4.4.0.0
    Microsoft.SPOT.Hardware,4.4.0.0
    Microsoft.SPOT.Hardware.Usb,4.4.0.0
    Microsoft.SPOT.Hardware.SerialPort,4.4.0.0
    Windows.Devices,4.4.0.0
    Microsoft.SPOT.Hardware.PWM,4.4.0.0