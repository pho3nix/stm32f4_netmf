/**
  ******************************************************************************
  * @file    STM32F407G_Discovery.Netmf.Hardware\STM32F407G_Discovery.Netmf.Hardware.cs
  * @author  Salvatore Alonge
  * @version V1.0.0
  * @date    16-Mar-2017
  ******************************************************************************
  */

using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Hardware.UsbClient;
using System.IO.Ports;
using System.Text;
using STM32F407G_Discovery.Netmf.Hardware;

namespace STM32F407G_Discovery.Netmf
{
    public class Board
    {
        public static Gpio gpio = new Gpio();
        public static Pwms pwms = new Pwms();
        public static Adc adc = new Adc();
        public UART uart = new UART(UART.COM1, 9600);
        public Pin[] pins = {

            new Pin("PA0",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT, Pin.MODES.ANALOG_INPUT),
            new Pin("PA1",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT, Pin.MODES.ANALOG_INPUT),
            new Pin("PA2",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT, Pin.MODES.ANALOG_INPUT),
            new Pin("PA3",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT, Pin.MODES.ANALOG_INPUT),

            new Pin("PA4",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),
            new Pin("PA5",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),
            new Pin("PA6",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),
            new Pin("PA7",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),

            new Pin("PA8",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),
            new Pin("PA9",Pin.MODES.GPIO_OUTPUT, Pin.MODES.LED),//LED green usb (no input)

            new Pin("PA13",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),
            new Pin("PA14",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),
            new Pin("PA15",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),

            //new Pin("PB6",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT, Pin.MODES.UART), UART TX (White)
            //new Pin("PB7",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT, Pin.MODES.UART), UART RX (Green)

            new Pin("PC0",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),
            new Pin("PC1",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),
            new Pin("PC2",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),
            new Pin("PC3",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),

            new Pin("PC4",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),
            new Pin("PC5",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),
            new Pin("PC6",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),
            new Pin("PC7",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT),

            new Pin("PD5", Pin.MODES.GPIO_OUTPUT, Pin.MODES.LED),//LED red usb (no input)

            new Pin("PD12",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT, Pin.MODES.PWM, Pin.MODES.LED),
            new Pin("PD13",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT, Pin.MODES.PWM, Pin.MODES.LED),
            new Pin("PD14",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT, Pin.MODES.PWM, Pin.MODES.LED),
            new Pin("PD15",Pin.MODES.GPIO_INPUT, Pin.MODES.GPIO_OUTPUT, Pin.MODES.PWM, Pin.MODES.LED),
        };

        public Board()
        {

        }
        public Pin getPin(string pinName) {
            foreach(Pin pin in pins)
            {
                if (pin.getName().Equals(pinName))
                {
                    return pin;
                }
            }
            return null;
        }
        public string readAllPins()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Pin pin in this.pins)
            {
                if (pin.getMode() == Pin.MODES.GPIO_INPUT)
                {
                    sb.AppendLine(pin.getName() + ":" + pin.getState());
                    Debug.Print("\nGPIO INPUT: " + pin.getName() + ": " + pin.getState());
                }
                if (pin.getMode() == Pin.MODES.ANALOG_INPUT)
                {
                    sb.AppendLine(pin.getName() + ":" + pin.getAnalog());
                    Debug.Print("\nANALOG INPUT: " + pin.getName() + ": " + pin.getAnalog());
                }
            }
            return sb.ToString();
        }
    }
    public class Pin
    {
        private int mode = -1;
        private string pinName;
        private int[] supportedModes;
        private Object pin;

        public Pin(string pinName, params int[] supportedModes)
        {
            this.supportedModes = supportedModes;
            this.pinName = pinName;
        }

        public string getName()
        {
            return pinName;
        }

        public int getMode()
        {
            return mode;
        }

        public Pin setMode(int mode)
        {
            if (this.mode == mode) return this;
            this.mode = -1;
            foreach (int m in supportedModes)
            {
                if (m == mode) this.mode = mode;
            }
            if (this.mode == -1) throw new Exception("Unsupported Pin Mode!");

            pin = null;
            if (this.mode == MODES.GPIO_INPUT)
            {
                pin = new InputPort(Board.gpio.get(pinName), false, Port.ResistorMode.PullUp);
            } else if (this.mode == MODES.GPIO_OUTPUT)
            {
                pin = new OutputPort(Board.gpio.get(pinName), false);
            } else if (this.mode == MODES.ANALOG_INPUT)
            {
                pin = new AnalogInput(Board.adc.get(pinName));
            } else if (this.mode == MODES.PWM)
            {
                pin = new PWM(Board.pwms.get(pinName), 0.1, 0.1, false);
            }
            return this;
        }

        public void setPWM(double freq, double dutycycle)
        {
            if (this.mode != MODES.PWM) throw new Exception("Unsupported method for current pin mode!");
            ((PWM)pin).Frequency = freq;
            ((PWM)pin).DutyCycle = dutycycle;
            ((PWM)pin).Start();
        }

        public double getAnalog()
        {
            if (this.mode != MODES.ANALOG_INPUT) throw new Exception("Unsupported method for current pin mode!");
            return ((AnalogInput)pin).Read();
        }

        public void setState(bool state)
        {
            if (this.mode != MODES.GPIO_OUTPUT || this.mode != MODES.LED) throw new Exception("Unsupported method for current pin mode!");
            ((OutputPort)pin).Write(state);
        }

        public bool getState()
        {
            if (this.mode != MODES.GPIO_INPUT) throw new Exception("Unsupported method for current pin mode!");
            return ((InputPort)pin).Read();
        }

        public static class MODES
        {
            public const int LED = 2;
            public const int GPIO_INPUT = 1;
            public const int GPIO_OUTPUT = 2;
            public const int ANALOG_INPUT = 3;
            public const int PWM = 4;
            public const int UART = 5;

        }
    }



}