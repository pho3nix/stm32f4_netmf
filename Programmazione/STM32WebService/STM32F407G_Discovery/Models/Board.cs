/**
  ******************************************************************************
  * @file    STM32F407G_Discovery.Netmf.Models\Board.cs
  * @author  Salvatore Alonge
  * @version V1.0.0
  * @date    31-Mar-2017
  ******************************************************************************
  */
using STM32F407G_Discovery.Netmf.Hardware;
using System.Collections;
using System;

namespace STM32F407G_Discovery.Netmf.Models
{
    public class Board
    {
        public UART uart = new UART(UART.COM1, 9600);
        private ArrayList pinout = new ArrayList();

        public Board()
        {
            pinout.Add(new BoardPin("PA0"));
            pinout.Add(new BoardPin("PA1"));
            pinout.Add(new BoardPin("PA2"));
            pinout.Add(new BoardPin("PA3"));

            pinout.Add(new BoardPin("PA4"));
            pinout.Add(new BoardPin("PA5"));
            pinout.Add(new BoardPin("PA6"));
            pinout.Add(new BoardPin("PA7"));

            pinout.Add(new BoardPin("PA8"));
            pinout.Add(new BoardPin("PA9"));
            pinout.Add(new BoardPin("PA10"));
            pinout.Add(new BoardPin("PA11"));

            pinout.Add(new BoardPin("PA12"));
            pinout.Add(new BoardPin("PA13"));
            pinout.Add(new BoardPin("PA14"));
            pinout.Add(new BoardPin("PA15"));

            pinout.Add(new BoardPin("PC0"));
            pinout.Add(new BoardPin("PC1"));
            pinout.Add(new BoardPin("PC2"));
            pinout.Add(new BoardPin("PC3"));

            pinout.Add(new BoardPin("PC4"));
            pinout.Add(new BoardPin("PC5"));
            pinout.Add(new BoardPin("PC6"));
            pinout.Add(new BoardPin("PC7"));
            
            pinout.Add(new BoardPin("PD12"));
            pinout.Add(new BoardPin("PD13"));
            pinout.Add(new BoardPin("PD14"));
            pinout.Add(new BoardPin("PD15"));
        }

        public BoardPin getPin(string pinName)
        {
            foreach (BoardPin pin in pinout)
            {
                if (pin.Name.Equals(pinName))
                {
                    return pin;
                }
            }
            throw new Exception("Unknown pin");
        }

        public ArrayList getPinout()
        {
            return pinout;
        }
    }
}
