# README #
## What is this repository for? ##

* Guida per l'installazione del NETMF (.NET Micro Framework) su STM32F4
* Testato su STM32F407G
* Version 0.1 [Salvatore Alonge]

## How do I get set up? ##

1. Installazione Microsoft Visual Studio 2015 (ad oggi l'ultima versione compatibile)
2. Installazione NETMF 4.4
3. Installazione NETMF VSIX Extensions
4. Installazione STM32 ST-LINK Utility
5. Flash su STM32F4 di Tinybooter (bootloader)
6. Installazione driver usb
7. Flash su STM32F4 di ERFLASH e ERCONFIG (CLR, PAL, HAL)
8. Debug da Microsoft Visual Studio su STM32F4

## Step-by-step guide ##

###STEP 1:  Installazione Microsoft Visual Studio 2015 (ad oggi l'ultima versione compatibile)###
Any version of Visual Studio 2015 can be used including the Visual Studio Express editions. 
Scaricare Visual Studio 2015 da https://www.microsoft.com/it-it/download/details.aspx?id=48146 (Visual Studio 2017  NON è attualmente supportato)
Lanciare l'installazione e selezionare il tipo di installazione custom. Tra i componenti da installare selezionare "Visual Studio Extendibility Tools" sotto la voce "Strumenti comuni".

###STEP 2: Installazione NETMF 4.4###
Uninstall any previous versions of the .NET Micro Framework SDK. Make sure you don't have any other versions of the .NET Micro Framework already installed. The .NET Micro Framework SDK cannot coexist with other versions of the SDK.
All'interno del progetto è già presente l'installer della .NET Micro Framework SDK 4.4 (STM32F4_NETMF / SDK Framework 4.4 / MicroFrameworkSDK.MSI). Oppure è possibile scaricare .NET Micro Framework SDK 4.4 da https://github.com/NETMF/netmf-interpreter/releases/tag/v4.4-RTW-20-Oct-2015

###STEP 3: Installazione NETMF VSIX Extensions###
In Visual Studio 2015: Strumenti -> Estensioni e aggiornamenti -> Installare .NET Micro Framework Project System
In Visual Studio 2015: Strumenti -> Estensioni e aggiornamenti -> Installare Visual Studio Extendibility Templates
Lanciare (avviandolo con un doppio click) il file vsix presente sul progetto (STM32F4_NETMF / SDK Framework 4.4 / NetMFVS14.vsix)

###STEP 4: Installazione STM32 ST-LINK Utility###
Installare STM32 ST-LINK Utility usando l'installer presente sul progetto (STM32F4_NETMF / Link-Utility / STM32 ST-LINK Utility v4.0.0 setup.exe)

###STEP 5: Flash su STM32F4 di Tinybooter (HAL) usando ST-LINK Utility###
Collegare la STM32F4 discovery al PC utilizzando l'interfaccia mini-usb.
Caricare la NETMF binary image (STM32F4_NETMF / STM32F4_NETMF_package / STM32F407_package / STM32F407_firmware_4.4 / Tinybooter.hex) sulla board, usando ST-LINK Utility:
In order to load the NETMF binary image into STM32F4 Discovery, you should do the following:
 - Open ST-LINK Utility.
 - Load hex file: File->Open file... (Ctrl+O)
 - Select Tinybooter.hex
 - Program the hex file: Target->Program & Verify (Ctrl+P)
ATTENZIONE!!! Il firmware (gli .hex) è diverso per ogni board! Il firmware della board STM32F429 non è compatibile con la board STM32F407.
ATTENZIONE!!! La versione del firmware deve coincidere con la versione del .NET Micro Framework! Un firmware 4.4 può essere utilizzato solo con una .NET Micro Framework 4.4.

###STEP 6: Installazione driver usb###
After loading the NETMF binary into the STM32F4 Discovery, install the NETMF USB driver.
a) Collegare la STM32F4 discovery al PC utilizzando sia l'interfaccia mini-usb (per l'alimentazione) sia l'interfaccia micro-usb (per la comunicazione). A device called "STM32F4 DISCOVERY" will be detected.
b) When windows asks for the driver, browse to the driver folder (STM32F4_NETMF / STM32F4_NETMF_package / STM32F407_package / driver /)
c) Once installation is done, an STM32F4_DISCOVERY NETMF will be displayed in the device manger.

###STEP 7: Flash su STM32F4 di ERFLASH e ERCONFIG (CLR)###
Collegare la STM32F4 discovery al PC utilizzando sia l'interfaccia mini-usb (per l'alimentazione) sia l'interfaccia micro-usb (per la comunicazione).
Aprire MFDeploy (.NET Micro Framework Deployment Tool), installato automaticamente con visual studio.
Nella sezione "Device":
	Selezionare dalla drop down list -> USB
	Nella successiva drop down list selezionare -> STM32F4DISCOVERY 
	Premere il pulsante "Ping". Se lo step 5 ha avuto successo il risultato dovrebbe essere "Pinging... Tinybooter".
Nella sezione "Image File":
	Premete il pulsante "Browse..." e selezionare i due file:
	stm32f4_netmf\STM32F4_NETMF_package\STM32F407_package\STM32F407_firmware_4.4\STM32F4_discovery_base_pwm_4.4\ER_FLASH.hex
	stm32f4_netmf\STM32F4_NETMF_package\STM32F407_package\STM32F407_firmware_4.4\STM32F4_discovery_base_pwm_4.4\ER_CONFIG.hex	
	Premere il pulsante Deploy per caricare gli hex sulla board.
Nella sezione "Device":
	Premere il pulsante "Ping". Se lo step 7 ha avuto successo il risultato dovrebbe essere "Pinging... TinyCLR".
ATTENZIONE!!! Gli hex contengono CLR e librerie. Quindi solo le librerie contenute negli hex possono essere utilizzate per la programmazione con visual studio. Se delle librerie non sono presenti sugli hex è necessario ricompilarli (https://github.com/NETMF/netmf-interpreter)
Due versioni sono incluse nel progetto:
	1. stm32f4_netmf\STM32F4_NETMF_package\STM32F407_package\STM32F407_firmware_4.4\STM32F4_discovery_base_4.4
		mscorlib,4.4.0.0
		Microsoft.SPOT.Native,4.4.0.0
		Microsoft.SPOT.Hardware,4.4.0.0
		Microsoft.SPOT.Hardware.Usb,4.4.0.0
		Microsoft.SPOT.Hardware.SerialPort,4.4.0.0
		Windows.Devices,4.4.0.0
	2. stm32f4_netmf\STM32F4_NETMF_package\STM32F407_package\STM32F407_firmware_4.4\STM32F4_discovery_base_pwm_4.4
		mscorlib,4.4.0.0
		Microsoft.SPOT.Native,4.4.0.0
		Microsoft.SPOT.Hardware,4.4.0.0
		Microsoft.SPOT.Hardware.Usb,4.4.0.0
		Microsoft.SPOT.Hardware.SerialPort,4.4.0.0
		Windows.Devices,4.4.0.0
		Microsoft.SPOT.Hardware.PWM,4.4.0.0
	
###STEP 8: Debug da Microsoft Visual Studio su STM32F4###
Da Visual Studio creare un nuovo progetto C# -> Micro Framework -> Console Application
Editare le proprietà del progetto.
Nella sezione  .NET Micro Framework selezionare USB nel campo Transport.
A questo punto nel campo device verrà mostrato il nome della board.