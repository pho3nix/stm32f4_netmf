/**
  ******************************************************************************
  * @file    STM32F407G_Discovery.Netmf.Hardware\STM32F407G_Discovery.Netmf.Hardware.cs
  * @author  Salvatore Alonge
  * @version V1.0.0
  * @date    16-Mar-2017
  ******************************************************************************
  */

using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Hardware.UsbClient;
using System.IO.Ports;
using System.Text;
using System.Collections;

namespace STM32F407G_Discovery.Netmf.Hardware
{
    ///<summary>
    ///This class provides set of functions to manage LEDs
    ///</summary>
    public class LED
    {
        public const Cpu.Pin GREEN_USB = (Cpu.Pin)9;
        public const Cpu.Pin RED_USB = (Cpu.Pin)53;
        public const Cpu.Pin GREEN = (Cpu.Pin)60;
        public const Cpu.Pin YELLOW = (Cpu.Pin)61;
        public const Cpu.Pin RED = (Cpu.Pin)62;
        public const Cpu.Pin BLUE = (Cpu.Pin)63;
        private OutputPort led;
        private Boolean status;
        public LED(Cpu.Pin pinLed)
        {
            led = new OutputPort(pinLed, false);
            status = false;
        }

        private void update()
        {
            led.Write(status);
        }

        public void toggle()
        {
            status = !status;
            update();
        }

        public void on()
        {
            status = true;
            update();
        }

        public void off()
        {
            status = false;
            update();
        }

    }
    
    public class UART
    {
        public const string COM1 = "COM1";
        public const string COM2 = "COM2";
        public const string COM3 = "COM3";
        public const string COM4 = "COM4";
        public const string COM5 = "COM5";
        public const string COM6 = "COM6";
        private SerialPort serial;
        public UART(string port, int speed)
        {
            serial = new SerialPort(port, speed);
            serial.Open();
        }
        public byte[] ReadByte()
        {
            byte[] rx_byte = new byte[1];
            int read_count = serial.Read(rx_byte, 0, 1);
            /*string instring = "";
            if (read_count > 0)
            {
                instring =  new String(Encoding.UTF8.GetChars(rx_byte));
            }*/
            return rx_byte;
        }
        public string ReadLine()
        {
            StringBuilder sb = new StringBuilder();
            while (true)
            {
                byte[] b = ReadByte();
                if (b[0] == 13) break;
                sb.Append(new String(Encoding.UTF8.GetChars(b)));
            }
            return sb.ToString();
        }
        public void Write(string outstring)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(outstring);
            serial.Write(buffer, 0, buffer.Length);
            serial.Flush();
        }
        public void WriteLine(string outstring)
        {
            Write(outstring+"/n");
        }
    }

    public class USB
    {
        private const int WRITE_EP = 1;
        private const int READ_EP = 2;
        private UsbStream usbStream;
        public USB()
        {
            // See if the hardware supports USB
            UsbController[] controllers = UsbController.GetControllers();
            // Bail out if USB is not supported on this hardware!
            if (0 == controllers.Length)
            {
                throw new Exception("USB is not supported on this hardware!");
            }
            // Find a free USB controller
            UsbController usbController = null;
            foreach (UsbController controller in controllers)
            {
                if (UsbController.PortState.Stopped == controller.Status)
                {
                    usbController = controller;
                    break;
                }
            }
            // If no free USB controller
            if (null == usbController)
            {
                throw new Exception("All available USB controllers already in use. Set the device to use Ethernet or Serial debugging.");
            }
            if (ConfigureUSBController(usbController))
            {
                usbStream = usbController.CreateUsbStream(WRITE_EP, READ_EP);
            }
            else
            {
                throw new Exception("USB stream could not be created, error " + usbController.ConfigurationError.ToString());
            }
        }

        public int Read(byte[] readData, int offset,int count)
        {
            return usbStream.Read(readData, offset, count);
        }

        public void Write(byte[] readData, int offset, int count)
        {
            usbStream.Write(readData, offset, count);
        }

        ~USB(){
            if (usbStream!= null) { 
                usbStream.Close();
            }
        }
        
        private bool ConfigureUSBController(UsbController usbController)
        {
            bool bRet = false;

            // Create the device descriptor
            Configuration.DeviceDescriptor device = new Configuration.DeviceDescriptor(0xDEAD, 0x0001, 0x0100);
            device.bcdUSB = 0x110;
            device.bDeviceClass = 0xFF;     // Vendor defined class
            device.bDeviceSubClass = 0xFF;     // Vendor defined subclass
            device.bDeviceProtocol = 0;
            device.bMaxPacketSize0 = 8;        // Maximum packet size of EP0
            device.iManufacturer = 1;        // String #1 is manufacturer name (see string descriptors below)
            device.iProduct = 2;        // String #2 is product name
            device.iSerialNumber = 3;        // String #3 is the serial number

            // Create the endpoints
            Configuration.Endpoint writeEP = new Configuration.Endpoint(WRITE_EP, Configuration.Endpoint.ATTRIB_Bulk | Configuration.Endpoint.ATTRIB_Write);
            writeEP.wMaxPacketSize = 64;
            writeEP.bInterval = 0;

            Configuration.Endpoint readEP = new Configuration.Endpoint(READ_EP, Configuration.Endpoint.ATTRIB_Bulk | Configuration.Endpoint.ATTRIB_Read);
            readEP.wMaxPacketSize = 64;
            readEP.bInterval = 0;

            Configuration.Endpoint[] usbEndpoints = new Configuration.Endpoint[] { writeEP, readEP };

            // Set up the USB interface
            Configuration.UsbInterface usbInterface = new Configuration.UsbInterface(0, usbEndpoints);
            usbInterface.bInterfaceClass = 0xFF; // Vendor defined class
            usbInterface.bInterfaceSubClass = 0xFF; // Vendor defined subclass
            usbInterface.bInterfaceProtocol = 0;

            // Create array of USB interfaces
            Configuration.UsbInterface[] usbInterfaces = new Configuration.UsbInterface[] { usbInterface };

            // Create configuration descriptor
            Configuration.ConfigurationDescriptor config = new Configuration.ConfigurationDescriptor(180, usbInterfaces);

            // Create the string descriptors
            Configuration.StringDescriptor manufacturerName = new Configuration.StringDescriptor(1, "ST Microelectronics");
            Configuration.StringDescriptor productName = new Configuration.StringDescriptor(2, "MicroFramework WinUSB");
            Configuration.StringDescriptor serialNumber = new Configuration.StringDescriptor(3, "0000-0000-0000-0001");
            Configuration.StringDescriptor displayName = new Configuration.StringDescriptor(4, "MicroFramework WinUSB");
            Configuration.StringDescriptor friendlyName = new Configuration.StringDescriptor(5, "NetMF_WinUSB");

            // Create the final configuration
            Configuration configuration = new Configuration();
            configuration.descriptors = new Configuration.Descriptor[]
            {
                    device,
                    config,
                    manufacturerName,
                    productName,
                    serialNumber,
                    displayName,
                    friendlyName
            };

            try
            {
                // Set the configuration
                usbController.Configuration = configuration;
                if (UsbController.ConfigError.ConfigOK != usbController.ConfigurationError)
                    throw new ArgumentException();
                // If all ok, start the USB controller.
                bRet = usbController.Start();
            }
            catch (ArgumentException)
            {
                throw new Exception("Can't configure USB controller, error " + usbController.ConfigurationError.ToString());
            }
            return bRet;
        }
    }
    
    public class Gpio
    {
        private Hashtable table = new Hashtable();
        public Gpio()
        {
            for (int i = 0; i <= 15; i++)
            {
                string p = "PA" + i % 16;
                if (i != 10 && i != 11 && i != 12)
                {
                    table.Add(p, (Cpu.Pin)i);
                }
            }
            for (int i = 16; i <= 31; i++)
            {
                string p = "PB" + i % 16;
                table.Add(p, (Cpu.Pin)i);
            }
            for (int i = 32; i <= 47; i++)
            {
                string p = "PC" + i % 16;
                table.Add(p, (Cpu.Pin)i);
            }
            for (int i = 48; i <= 63; i++)
            {
                string p = "PD" + i % 16;
                table.Add(p, (Cpu.Pin)i);
            }
            for (int i = 64; i <= 79; i++)
            {
                string p = "PE" + i % 16;
                table.Add(p, (Cpu.Pin)i);
            }
        }
        public Cpu.Pin get(string key)
        {
            if (table.Contains(key))
            {
                return (Cpu.Pin)table[key];
            }
            throw new Exception("Unsupported GPIO pin");
        }
        public static class GPIO_PINS
        {
            public const Cpu.Pin PA0 = (Cpu.Pin)0; //User button
            public const Cpu.Pin PA1 = (Cpu.Pin)1;
            public const Cpu.Pin PA2 = (Cpu.Pin)2;
            public const Cpu.Pin PA3 = (Cpu.Pin)3;

            public const Cpu.Pin PA4 = (Cpu.Pin)4;
            public const Cpu.Pin PA5 = (Cpu.Pin)5;
            public const Cpu.Pin PA6 = (Cpu.Pin)6;
            public const Cpu.Pin PA7 = (Cpu.Pin)7;

            public const Cpu.Pin PA8 = (Cpu.Pin)8;
            public const Cpu.Pin PA9 = (Cpu.Pin)9; //USB led green
            //public const Cpu.Pin PA10 = (Cpu.Pin)10; Dedicato a USB
            //public const Cpu.Pin PA11 = (Cpu.Pin)11; Dedicato a USB
            //public const Cpu.Pin PA12 = (Cpu.Pin)12; Dedicato a USB

            public const Cpu.Pin PA13 = (Cpu.Pin)13;
            public const Cpu.Pin PA14 = (Cpu.Pin)14;
            public const Cpu.Pin PA15 = (Cpu.Pin)15;

            public const Cpu.Pin PB0 = (Cpu.Pin)16;
            public const Cpu.Pin PB1 = (Cpu.Pin)17;
            public const Cpu.Pin PB2 = (Cpu.Pin)18;
            public const Cpu.Pin PB3 = (Cpu.Pin)19;

            public const Cpu.Pin PB4 = (Cpu.Pin)20;
            public const Cpu.Pin PB5 = (Cpu.Pin)21;
            public const Cpu.Pin PB6 = (Cpu.Pin)22;
            public const Cpu.Pin PB7 = (Cpu.Pin)23;

            public const Cpu.Pin PB8 = (Cpu.Pin)24;
            public const Cpu.Pin PB9 = (Cpu.Pin)25;
            public const Cpu.Pin PB10 = (Cpu.Pin)26;
            public const Cpu.Pin PB11 = (Cpu.Pin)27;

            public const Cpu.Pin PB12 = (Cpu.Pin)28;
            public const Cpu.Pin PB13 = (Cpu.Pin)29;
            public const Cpu.Pin PB14 = (Cpu.Pin)30;
            public const Cpu.Pin PB15 = (Cpu.Pin)31;
            
            public const Cpu.Pin PC0 = (Cpu.Pin)32;
            public const Cpu.Pin PC1 = (Cpu.Pin)33;
            public const Cpu.Pin PC2 = (Cpu.Pin)34;
            public const Cpu.Pin PC3 = (Cpu.Pin)35;

            public const Cpu.Pin PC4 = (Cpu.Pin)36;
            public const Cpu.Pin PC5 = (Cpu.Pin)37;
            public const Cpu.Pin PC6 = (Cpu.Pin)38;
            public const Cpu.Pin PC7 = (Cpu.Pin)39;

            public const Cpu.Pin PC8 = (Cpu.Pin)40;
            public const Cpu.Pin PC9 = (Cpu.Pin)41;
            public const Cpu.Pin PC10 = (Cpu.Pin)42;
            public const Cpu.Pin PC11 = (Cpu.Pin)43;

            public const Cpu.Pin PC12 = (Cpu.Pin)44;
            public const Cpu.Pin PC13 = (Cpu.Pin)45;
            public const Cpu.Pin PC14 = (Cpu.Pin)46;
            public const Cpu.Pin PC15 = (Cpu.Pin)47;

            public const Cpu.Pin PD0 = (Cpu.Pin)48;
            public const Cpu.Pin PD1 = (Cpu.Pin)49;
            public const Cpu.Pin PD2 = (Cpu.Pin)50;
            public const Cpu.Pin PD3 = (Cpu.Pin)51;

            public const Cpu.Pin PD4 = (Cpu.Pin)52;
            public const Cpu.Pin PD5 = (Cpu.Pin)53; //USB led red
            public const Cpu.Pin PD6 = (Cpu.Pin)54;
            public const Cpu.Pin PD7 = (Cpu.Pin)55;

            public const Cpu.Pin PD8 = (Cpu.Pin)56;
            public const Cpu.Pin PD9 = (Cpu.Pin)57;
            public const Cpu.Pin PD10 = (Cpu.Pin)58;
            public const Cpu.Pin PD11 = (Cpu.Pin)59;

            public const Cpu.Pin PD12 = (Cpu.Pin)60; //LED Green
            public const Cpu.Pin PD13 = (Cpu.Pin)61; //LED Yellow
            public const Cpu.Pin PD14 = (Cpu.Pin)62; //LED Red
            public const Cpu.Pin PD15 = (Cpu.Pin)63; //LED Blue

            public const Cpu.Pin PE0 = (Cpu.Pin)64;
            public const Cpu.Pin PE1 = (Cpu.Pin)65;
            public const Cpu.Pin PE2 = (Cpu.Pin)66;
            public const Cpu.Pin PE3 = (Cpu.Pin)67;

            public const Cpu.Pin PE4 = (Cpu.Pin)68;
            public const Cpu.Pin PE5 = (Cpu.Pin)69;
            public const Cpu.Pin PE6 = (Cpu.Pin)70;
            public const Cpu.Pin PE7 = (Cpu.Pin)71;

            public const Cpu.Pin PE8 = (Cpu.Pin)72;
            public const Cpu.Pin PE9 = (Cpu.Pin)73;
            public const Cpu.Pin PE10 = (Cpu.Pin)74;
            public const Cpu.Pin PE11 = (Cpu.Pin)75;

            public const Cpu.Pin PE12 = (Cpu.Pin)76;
            public const Cpu.Pin PE13 = (Cpu.Pin)77;
            public const Cpu.Pin PE14 = (Cpu.Pin)78;
            public const Cpu.Pin PE15 = (Cpu.Pin)79;
                       
        }
    }
    public class Adc
    {
        private Hashtable table = new Hashtable();
        public Adc()
        {
            table.Add("PA0", PINS.PA0);
            table.Add("PA1", PINS.PA1);
            table.Add("PA2", PINS.PA2);
            table.Add("PA3", PINS.PA3);
        }
        public Cpu.AnalogChannel get(string key)
        {
            if (table.Contains(key))
            {
                return (Cpu.AnalogChannel)table[key];
            }
            throw new Exception("Unsupported Analog pin");
        }
        public static class PINS
        {
            public const Cpu.AnalogChannel PA0 = Cpu.AnalogChannel.ANALOG_0;
            public const Cpu.AnalogChannel PA1 = Cpu.AnalogChannel.ANALOG_1;
            public const Cpu.AnalogChannel PA2 = Cpu.AnalogChannel.ANALOG_2;
            public const Cpu.AnalogChannel PA3 = Cpu.AnalogChannel.ANALOG_3;
        }
    }

    public class Pwms
    {
        private Hashtable table = new Hashtable();
        public Pwms()
        {
            table.Add("PD12", PINS.PD12);
            table.Add("PD13", PINS.PD13);
            table.Add("PD14", PINS.PD14);
            table.Add("PD15", PINS.PD15);
        }
        public Cpu.PWMChannel get(string key)
        {
            if (table.Contains(key))
            {
                return (Cpu.PWMChannel)table[key];
            }
            throw new Exception("Unsupported PWM pin");
        }
        public class PINS
        {
            //Stesso pin (60) di led verde e GPIO PD12 (uso esclusivo)
            public const Cpu.PWMChannel PWM0 = Cpu.PWMChannel.PWM_0;
            public const Cpu.PWMChannel PD12 = Cpu.PWMChannel.PWM_0;

            //Stesso pin (61) di led giallo e GPIO PD13 (uso esclusivo)
            public const Cpu.PWMChannel PWM1 = Cpu.PWMChannel.PWM_1;
            public const Cpu.PWMChannel PD13 = Cpu.PWMChannel.PWM_1;

            //Stesso pin (62) di led rosso e GPIO PD14 (uso esclusivo)
            public const Cpu.PWMChannel PWM2 = Cpu.PWMChannel.PWM_2;
            public const Cpu.PWMChannel PD14 = Cpu.PWMChannel.PWM_2;

            //Stesso pin (63) di led blue e GPIO PD15 (uso esclusivo)
            public const Cpu.PWMChannel PWM3 = Cpu.PWMChannel.PWM_3;
            public const Cpu.PWMChannel PD15 = Cpu.PWMChannel.PWM_3;
        }
    }
}
