﻿using STM32Commons.Models;
using STM32WebService.Utils;
using System.Collections.Generic;
using STM32Commons.Utils;
using System;
using System.Collections;
using System.Linq;

namespace STM32WebService.Repositories
{
    public class PinRepository : IPinRepository
    {
        private MessageHandler handler;

        public PinRepository()
        {
            SerialUtils serial = new SerialUtils("COM6", 9600);
            handler = new MessageHandler(serial);
        }

        public Pin getPin(string pinName)
        {
            Message request = new Message();
            request.Opcode = Commands.GET_PIN;
            request.Lenght = 1;
            request.Payload = pinName;

            Message response = handler.sendSync(request);

            Pin result = null;
            if (response.Payload != null)
            {
                result = (Pin)response.Payload;
            }

            return result;
        }

        public List<Pin> getPins()
        {
            Message request = new Message();
            request.Opcode = Commands.GET_PINS;
            request.Lenght = 0;

            Message response = handler.sendSync(request);

            List<Pin> result = new List<Pin>();
            if (response.Payload != null)
            {
                result = ((ArrayList)response.Payload).Cast<Pin>().ToList();
            }

            return result;
        }
        
        public Pin setPin(Pin pin)
        {
            Message request = new Message();
            request.Opcode = Commands.SET_PIN;
            request.Lenght = 1;
            request.Payload = pin;

            Message response = handler.sendSync(request);

            return (Pin)response.Payload;
        }

        public PinValue getValue(string pinName)
        {
            Message request = new Message();
            request.Opcode = Commands.READ_PIN;
            request.Lenght = 1;
            request.Payload = pinName;

            Message response = handler.sendSync(request);

            PinValue result = new PinValue();
            if (response.Payload!=null)
            {
                result = (PinValue)response.Payload;
            }

            return result;
        }

        public PinValue setValue(PinValue value)
        {
            Message request = new Message();
            request.Opcode = Commands.READ_PIN;
            request.Lenght = 1;
            request.Payload = value;

            Message response = handler.sendSync(request);

            return (PinValue)response.Payload;
        }

        public List<PinValue> getValues()
        {
            Message request = new Message();
            request.Opcode = Commands.READ_PINS;
            request.Lenght = 0;

            Message response = handler.sendSync(request);

            List<PinValue> result = new List<PinValue>();
            if (response.Payload != null)
            {
                result = ((ArrayList)response.Payload).Cast<PinValue>().ToList();
            }

            return result;
        }
    }
}
