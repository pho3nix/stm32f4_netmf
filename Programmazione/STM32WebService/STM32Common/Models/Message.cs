using System;

namespace STM32Commons.Models
{
    class Message
    {
        public Message() { }

        public Message(string opcode, Object payload, int lenght)
        {
            this.Opcode = opcode;
            this.Payload = payload;
            this.Lenght = lenght;
        }

        public string Opcode { get; set; }
        public Object Payload { get; set; }
        public int Lenght { get; set; }

    }
}
