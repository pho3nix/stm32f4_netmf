using System;
using System.Collections;
using Microsoft.SPOT.Hardware;

namespace STM32F407G_Discovery.Netmf.Hardware
{
    public static class PWMS
    {
        public static Hashtable pins = getPins();
        private static Hashtable getPins()
        {
            Hashtable pins = new Hashtable();
            //Stesso pin (60) di led verde e GPIO PD12 (uso esclusivo)
            pins.Add("PD12", Cpu.PWMChannel.PWM_0);

            //Stesso pin (61) di led giallo e GPIO PD13 (uso esclusivo)
            pins.Add("PD13", Cpu.PWMChannel.PWM_1);

            //Stesso pin (62) di led rosso e GPIO PD14 (uso esclusivo)
            pins.Add("PD14", Cpu.PWMChannel.PWM_2);

            //Stesso pin (63) di led blue e GPIO PD15 (uso esclusivo)
            pins.Add("PD15", Cpu.PWMChannel.PWM_3);
            return pins;
        }
        public static Cpu.PWMChannel get(string key)
        {
            if (pins.Contains(key))
            {
                return (Cpu.PWMChannel)pins[key];
            }
            throw new Exception("Unsupported PWM pin");
        }
        public static bool contains(string key)
        {
            return pins.Contains(key);
        }
    }
}
