using System;
using System.IO.Ports;
using System.Text;

namespace STM32F407G_Discovery.Netmf.Hardware
{
    public class UART
    {
        // currently supported only port com1
        public const string COM1 = "COM1";
        //pins.Add("PB6", (Cpu.Pin)22); // UART TX (White)
        //pins.Add("PB7", (Cpu.Pin)23); // UART RX (Green)

        private SerialPort serial;
        public UART(string port, int speed)
        {
            serial = new SerialPort(port, speed);
            serial.Open();
        }

        public byte[] ReadByte()
        {
            byte[] rx_byte = new byte[1];
            int read_count = serial.Read(rx_byte, 0, 1);
            return rx_byte;
        }

        public string ReadLine()
        {
            StringBuilder sb = new StringBuilder();
            while (true)
            {
                byte[] b = ReadByte();
                if (b[0] == 13) break;
                sb.Append(new String(Encoding.UTF8.GetChars(b)));
            }
            return sb.ToString();
        }

        public void WriteByte(byte[] tx_byte)
        {
            serial.Write(tx_byte, 0, tx_byte.Length);
            serial.Flush();
        }

        public void Write(string outstring)
        {
            byte[] tx_byte = Encoding.UTF8.GetBytes(outstring);
            WriteByte(tx_byte);
        }

        public void WriteLine(string outstring)
        {
            Write(outstring + "\r\n");
        }
    }
}
