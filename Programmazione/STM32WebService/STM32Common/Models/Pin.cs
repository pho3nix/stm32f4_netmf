﻿using System;

namespace STM32Commons.Models
{
    public class Pin
    {
        public Pin(string name)
        {
            Name = name;
            SupportedModes = null;
            Mode = -1;
            Frequency = 0;
        }

        public Pin(string name, int[] supportedModes) : this(name)
        {
            SupportedModes = supportedModes;
        }

        public string Name { get; protected set; }
        public bool Enabled { get; protected set; }
        public int[] SupportedModes { get; protected set; }

        public int Frequency { get; set; }
        public int Mode
        {
            get
            {
                return Mode;
            }
            set
            {
                if (Mode == value) return;
                disable(); //Set Mode = -1, Enabled = false
                if (value == -1)
                {
                    return;
                }
                if (SupportedModes == null) throw new Exception("No supported modes specified");
                foreach (int m in SupportedModes)
                {
                    if (m == value) Mode = value;
                }
                if (Mode == -1) throw new Exception("Unsupported pin mode");
                enable(); //Set Enabled = true
            }
        }

        protected virtual void enable()
        {
            Enabled = true;
        }

        protected virtual void disable()
        {
            Mode = -1;
            Enabled = false;
        }
    }
}
