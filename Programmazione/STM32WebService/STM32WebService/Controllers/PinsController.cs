﻿using STM32Commons.Models;
using STM32WebService.Repositories;
using System.Collections.Generic;
using System.Web.Http;

namespace STM32WebService.Controllers
{
    public class PinsController : ApiController
    {
        static IPinRepository repository = new PinRepository();

        // GET: api/Pin/get
        [HttpGet]
        public IEnumerable<Pin> Get()
        {
            return repository.getPins();
        }

        // GET: api/Pin/get/5
        [HttpGet]
        public Pin Get(string id)
        {
            return repository.getPin(id);
        }

        // GET: api/Pin/read
        [HttpGet]
        public IEnumerable<PinValue> Read()
        {
            return repository.getValues();
        }

        // GET: api/Pin/read/5
        [HttpGet]
        public PinValue Read(string id)
        {
            return repository.getValue(id);
        }

        // PUT: api/Pin/put
        [HttpPut]
        public Pin Put(Pin pin)
        {
            return repository.setPin(pin);
        }
        
        // DELETE: api/Pin/5
        /*
        public void Delete(int id)
        {
        }*/

        // POST: api/Pin
        /*public Pin Post([FromBody]Pin value)
        {
            Pin res = repository.setPin(value);
            return res;
        }*/
    }
}
