using Microsoft.SPOT.Hardware;
using System;
using System.Collections;

namespace STM32F407G_Discovery.Netmf.Hardware
{
    public static class BUTTON
    {
        public static Hashtable pins = getPins();
        private static Hashtable getPins()
        {
            Hashtable pins = new Hashtable();
            pins.Add("PA0", (Cpu.Pin)0); //User button
            return pins;
        }
        public static Cpu.Pin get(string key)
        {
            if (pins.Contains(key))
            {
                return (Cpu.Pin)pins[key];
            }
            throw new Exception("Unsupported BUTTON pin");
        }
        public static bool contains(string key)
        {
            return pins.Contains(key);
        }
    }
}
