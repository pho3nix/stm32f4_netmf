﻿namespace STM32Commons.Utils
{
    public static class Commands
    {
        public const string ERROR = "Error";

        public const string GET_PINS = "GetPins";
        public const string GET_PIN = "GetPin";
        public const string SET_PIN = "SetPin";

        public const string READ_PINS = "ReadPins";
        public const string READ_PIN = "ReadPin";
        public const string WRITE_PIN = "WritePin";

    }
}
