using System;
using STM32F407G_Discovery.Netmf.Models;
using STM32Commons.Models;
using STM32Commons.Utils;
using System.Collections;

namespace STM32F407G_Discovery.Netmf.Utils
{
    class MessageHandler
    {
        private Board board;

        public MessageHandler(Board board)
        {
            this.board = board;
        }

        public Message handleMessage(Message request)
        {
            Message response = new Message();
            if (request == null || request.Opcode == null)
            {
                throw new Exception("Empty or invalid Request");
            }
            else
            {
                switch (request.Opcode)
                {
                    case Commands.GET_PINS:
                        {
                            response.Opcode = Commands.GET_PINS;
                            ArrayList pinout = board.getPinout();
                            response.Payload = pinout;
                            response.Lenght = pinout.Count;
                            break;
                        }
                    case Commands.GET_PIN:
                        {
                            response.Opcode = Commands.GET_PIN;
                            string pinName = request.Payload as string;
                            BoardPin pin = board.getPin(pinName);
                            response.Payload = pin;
                            response.Lenght = 1;
                            break;
                        }
                    case Commands.SET_PIN:
                        {
                            response.Opcode = Commands.SET_PIN;
                            Pin pin = request.Payload as Pin;
                            BoardPin bpin = board.getPin(pin.Name);
                            bpin.Mode = pin.Mode;
                            bpin.Frequency = pin.Frequency;
                            response.Payload = bpin;
                            response.Lenght = 1;
                            break;
                        }
                    case Commands.READ_PIN:
                        {
                            response.Opcode = Commands.READ_PIN;
                            string pinName = request.Payload as string;
                            response.Payload = board.getPin(pinName).Read();
                            response.Lenght = 1;
                            break;
                        }
                    case Commands.READ_PINS:
                        {
                            response.Opcode = Commands.READ_PINS;
                            ArrayList values = new ArrayList();
                            foreach (BoardPin pin in board.getPinout())
                            {
                                try {
                                    PinValue value = pin.Read();
                                    values.Add(value);
                                }
                                catch (Exception ex)
                                {
                                    //Cannot read this pin
                                }
                            }
                            response.Payload = values;
                            response.Lenght = values.Count;
                            break;
                        }
                    default:
                        throw new Exception("Unknown opcode");
                }
            }
            return response;

        }
    }
}
