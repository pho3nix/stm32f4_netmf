﻿using STM32Commons.Models;
using System.Collections.Generic;

namespace STM32WebService.Repositories
{
    interface IPinRepository
    {
        //Configurazione
        List<Pin> getPins();
        Pin getPin(string pinName);
        Pin setPin(Pin pin);

        List<PinValue> getValues();
        PinValue getValue(string pinName);
        PinValue setValue(PinValue pinValue);

    }
}
